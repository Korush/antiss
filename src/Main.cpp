#include "windows.h"
#include "resource.h"

BOOL CALLBACK DialogProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam);

int ErrorHandleProc();

wchar_t C_ON[] = L"W��CZONE";
wchar_t C_OFF[] = L"WY��CZONE";
bool IS_TURNED = false;

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, char* cmdParam, int cmdShow)
{
	HWND dialogBox = nullptr;
	dialogBox = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOGBAR), 0, DialogProc);

	if(dialogBox == nullptr)
	{
		return ErrorHandleProc();
	}
	SetTimer(dialogBox, 99, 10000, nullptr);
	ShowWindow(dialogBox, SW_SHOW);

	BOOL bRet = FALSE;
	MSG msg;
	while((bRet = GetMessage(&msg, NULL, 0, 0)) != 0)
	{
		if(bRet == -1)
		{
			return ErrorHandleProc();
		}
		else if(!IsWindow(dialogBox) || !IsDialogMessage(dialogBox, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	return 0;
}

BOOL CALLBACK DialogProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message)
	{
		case WM_COMMAND:
		{
			switch (LOWORD(wParam))
			{
			case IDC_BUTTON:
			{
				HWND hButton = GetDlgItem(hwndDlg, IDC_BUTTON);
				if (IS_TURNED)
				{
					SetWindowText(hButton, C_OFF);
				}
				else
				{
					SetWindowText(hButton, C_ON);
				}
				IS_TURNED = !IS_TURNED;
			}
				return TRUE;
			case IDCANCEL:
				SendMessage(hwndDlg, WM_CLOSE, 0, 0);
				return TRUE;
			}
		}
		case WM_TIMER:
			if(IS_TURNED)
			{
				SetThreadExecutionState(ES_DISPLAY_REQUIRED);
			}
			return TRUE;
		case WM_DESTROY:
			PostQuitMessage(0);
			return TRUE;
		case WM_CLOSE:
			DestroyWindow(hwndDlg);
			hwndDlg = nullptr;
			return TRUE;
	}
	return FALSE;
}

int ErrorHandleProc()
{
	wchar_t buff[100];
	wsprintf(buff, L"Error %lu", GetLastError());
	MessageBox(0, buff, L"Error occured", MB_ICONEXCLAMATION | MB_OK);
	PostQuitMessage(1);
	return 1;
}
